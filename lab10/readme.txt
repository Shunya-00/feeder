Group 00
Shunya
Lab 10

(Bharat Khandelwal, 150050001) (Rishabh Shah,150050006) (Purav Gandhi, 150050007)

I pledge on my honour that I have not given or received any unauthorized assistance on this assignment or any previous task.
															-Bharat Khandelwal
I pledge on my honour that I have not given or received any unauthorized assistance on this assignment or any previous task.
															-Rishabh Shah
I pledge on my honour that I have not given or received any unauthorized assistance on this assignment or any previous task.
															-Purav Gandhi

Individual Contributions:
Student 1: 150050001 - 100%
Student 2: 150050006 - 100%
Student 3: 150050007 - 100%

Notes to the Instructor:-

https://bitbucket.org/Shunya-00/feeder

Commit ID -  adeeda3

-To start the server into motion navigate to lab10/webServer and write -
	$ python3 manage.py runserver

-admin login Page:	http://localhost:8000/webApp/login/
	admin credentials:
		username: admin
		password: administrator


ExtraFeatures
	- Created a change password feature for admin
	- Had Created a Feedback Form that allows for adding any no. of questions dynamically but since it was later told that admin won't add them
		we didn't provide a link to that in navigation, yet here's the link - http://localhost:8000/webApp/feedbacknew/

Citation:-
	Materialize CSS - http://materializecss.com/
	DjangoTutorials -  https://docs.djangoproject.com/en/1.10/intro/tutorial01/
	Extending user models - http://www.b-list.org/weblog/2006/jun/06/django-tips-extending-user-model/

Reflection Essay:
	Learnt to handle post and get server requests and transfer of information across webpages.
	Learnt use of materialize and its advantages over bootstrap
	Learnt handling authentication and  maintaining sessions in django.
	Learnt to write Sql quries in python.
	Enjoyed creating our own server and maintaing database.
