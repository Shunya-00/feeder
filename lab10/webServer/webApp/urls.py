from django.conf.urls import url

from . import views

app_name = 'webApp'
urlpatterns = [
	url(r'login/', views.login, name='login'),
	url(r'logout/', views.logout, name='logout'),
    url(r'signup/', views.signup, name='signup'),

    url(r'settings/', views.settings, name='settings'),
    url(r'home/', views.home, name='home'),

    url(r'courses/', views.courses, name='courses'),
	url(r'deadlines/', views.deadlines, name='deadlines'),
    url(r'feedback/', views.feedback, name='feedback'),
    url(r'coursesnew/', views.coursesnew, name='coursesnew'),
	url(r'deadlinesnew/', views.deadlinesnew, name='deadlinesnew'),
	url(r'feedbacknew/', views.feedbacknew, name='feedbacknew'),
    url(r'studentInitialise/', views.studentInitialise, name='studentInitialise'),


    url(r'api/', views.android_data, name='andy'),

]
