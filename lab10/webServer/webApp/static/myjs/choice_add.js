var counter = 1;
var limit = 5;

function addInput(){
  if (counter == limit)  {
    alert("You have reached the limit of adding " + counter + " inputs");
  }

  else {
    var newdiv = document.createElement('div');
      newdiv.setAttribute('class','input-field');
    var newlabel = document.createElement('label');
      newlabel.setAttribute('for','question'+(counter+1).toString());
      newlabel.innerHTML = "Question " + (counter+1).toString();
    var newinput = document.createElement('input');
      newinput.setAttribute('required','');
      newinput.setAttribute('aria-required','true');
      newinput.setAttribute('name','question'+(counter+1).toString());
      newinput.setAttribute('id','question'+(counter+1).toString());
      newinput.setAttribute('type','text');
      newinput.setAttribute('class','validate');

    newdiv.appendChild(newlabel);
    newdiv.appendChild(newinput);

    counter++;
    document.getElementById("Questions").appendChild(newdiv);
    document.getElementById("noQ").setAttribute('value',counter);

    if(counter==limit)
      document.getElementById("addButton").setAttribute('class','disabled btn-floating waves-effect waves-light');
   }
}