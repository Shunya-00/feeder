$(document).ready(function() { $('select').material_select(); });
     $('.datepicker').pickadate({
        selectMonths: true, // Creates a dropdown to control month
        selectYears: 3,
        format: 'yyyy-mm-dd'
      });

    function valid_date() {
      var val = document.getElementById('date').value;
      if(!Date.parse(val)){
        alert('Submission date cannot be empty');
        return false;
      }
      return true;
    }
    function valid_date_all() {
      var val1 = document.getElementById('date1').value;
      var val2 = document.getElementById('date2').value;
      var val3 = document.getElementById('date3').value;
      var val4 = document.getElementById('date4').value;
      
      if(!Date.parse(val1)){
        alert('Last Date for Midsemester Feedback submission cannot be empty');
        return false;
      }
      if(!Date.parse(val2)){
        alert('Last Date for Endsemester Feedback submission cannot be empty');
        return false;
      }
      if(!Date.parse(val3)){
        alert('Date for Midsemester Examination cannot be empty');
        return false;
      }
      if(!Date.parse(val4)){
        alert('Date for Endsemester Examination cannot be empty');
        return false;
      }
      return true;
    }