var success ="#00FA9A";
var failure ="#ee6e73";

function encode(str ,start){
	var code = {};
	code['&'] = "&amp;";
	code['<'] = "&lt;";
	code['>'] = "&gt;";
	code['"'] = "&quot;";
	code["'"] = "&#x27;";
	code["/"] = "&#x2F;";

	for(var i=start;i<str.length;i++){
		if(str[i]=='&' || str[i]=='<' || str[i]=='>' || str[i]=='"'|| str[i]=="'" || str[i]=="/"){
			var left = str.substring(0,i);
			var right = str.substring(i+1,str.length);
			str = left + code[str[i]] + right;
		}
	}
	return str;
}
function validate_text(){
	var inputText =document.querySelector("#Text").value;
	if(inputText.length < 5 || inputText.length > 20){
		document.querySelector("#Text").style.borderColor=failure;
		document.querySelector("#ErrText").innerHTML = "Number Of Charcters should be between 5 and 20";	
		return false;
	}
	document.querySelector("#Text").style.borderColor=success;	
	document.querySelector("#ErrText").innerHTML = "";
	return true;
}
function validate_password(){
	var inputPassword = document.querySelector("#Pass").value;
	if(inputPassword.length < 8 || inputPassword.length > 20){
		document.querySelector("#Pass").style.borderColor=failure;
		document.querySelector("#ErrPass").innerHTML = "Number Of Charcters should be between 8 and 20";
		return false;
	}
	else{
		document.querySelector("#Pass").style.borderColor=success;
		document.querySelector("#ErrPass").innerHTML = "";
		return true;
	}
	document.querySelector("#ErrPass").innerHTML = "Include atleast one special character { $ , ! , @ , _}";
	document.querySelector("#Pass").style.borderColor=failure;
	return false;
}
function validate_email(){
	var inputEmail =document.querySelector("#Email").value;

	var str; var valid=false;
	
	var re = /[a-zA-Z0-9._]+@[a-zA-Z]+\./;
	var n = inputEmail.search(re);
	inputEmail +=".";
	if(n==0){
		var posOfAT=0;
		var posOfDec=0;

		for (var i = 0; i <= inputEmail.length - 1; i++) {
			if(inputEmail[i]=='@'){
				if(i <3) break;
				else{
					posOfAT=i;
					posOfDec=i;
				}
			}
			if(posOfAT > 0){
				if(inputEmail[i]=='.'){
					if(posOfDec==posOfAT){
						if(i- posOfAT <= 3) break;
					} 
					else{
						if( i- posOfDec <= 2) break;
						else{
							str = inputEmail.substring(posOfDec,i+1);
							re =/\.[a-zA-Z]+\./;
							n = str.search(re);
							if(n!=0) break;
						}
					}
					posOfDec =i;
				}
			}
			if(i==inputEmail.length-1){
				document.querySelector("#Email").style.borderColor=success;
				document.querySelector("#ErrEmail").innerHTML="";
				return true;
			}
		}
	}
	document.querySelector("#Email").style.borderColor="#f2dede";
	document.querySelector("#ErrEmail").innerHTML="Invalid Email Address";
	return false;
}
function validate_check(){
	var inputNewPassword = document.querySelector("#Pass").value;
	var valid = validate_password();
	if(!valid)
		return false;
	var inputretypePassword = document.querySelector("#reNewPass").value;
	
	if(inputretypePassword!=inputNewPassword){
		document.querySelector("#Pass").style.borderColor=failure;
		document.querySelector("#ErrPass").innerHTML = "New Passwords don't match";
		return false;
	}
	else{
		document.querySelector("#Pass").style.borderColor=success;
		document.querySelector("#reNewPass").style.borderColor=success;
		document.querySelector("#ErrPass").innerHTML = "";
		return true;
	}
}
function validate_all() { 
	var allValid = validate_text() && validate_email()&& validate_password() ;
	if(allValid){
		var inputText,inputPassword,inputEmail;
		inputPassword = document.querySelector("#Pass").value;
		inputText = document.querySelector("#Text").value;
		inputEmail = document.querySelector("#Email").value;

		// inputText = encode(inputText,0);
		// inputPassword = encode(inputPassword,0);
		document.querySelector("#Text").value =inputText;
		document.querySelector("#Pass").value =inputPassword;

		//alert("All Valid");
	}
	return allValid;
}

