#for basic rendering of html pages
from django.shortcuts import render, render_to_response
from django.template import RequestContext
from django.http import HttpResponse, HttpResponseRedirect

#for authentication login and logout
from django.contrib.auth import login as auth_login
from django.contrib.auth import logout as auth_logout
from django.contrib.auth import authenticate
from django.contrib.auth.decorators import login_required

#models
from django.contrib.auth.models import User
from .models import *

#read student data
import csv
import json
#Logging in ang signing up
def login(request):
	if request.POST :
		user = authenticate(username=request.POST['name'], password=request.POST['pass'])
		if user is not None:
			if user.is_active:
				auth_login(request, user)
				return HttpResponseRedirect('/webApp/home/')
		return render(request, "webApp/login.html",context={'error':'invalid credentials'})
	else:
		if(request.user.is_authenticated):
			return HttpResponseRedirect('/webApp/home/')
		else:
			return render(request, "webApp/login.html",context={'error':''})

def signup(request):
	if request.POST :
		x= User.objects.create_user(request.POST['name'],
			request.POST['email'], request.POST['pass'])
		x.save();
		auth_login(request,x)
		return HttpResponseRedirect('/webApp/home/')

	else:
		if(request.user.is_authenticated):
			return HttpResponseRedirect('/webApp/home/')
		else:
			return render(request, "webApp/signup.html")


#Home Page related in navigation bar
@login_required()
def home(request):
	return render(request, "webApp/home.html")

@login_required()
def logout(request):
	data={'name':request.user.username}
	auth_logout(request)
	return render(request, "webApp/logout.html",context=data)

@login_required()
def settings(request):
	if request.POST :
		u = User.objects.get(username__exact=request.user.username)
		if not u.check_password(request.POST['currPass']) :
			return render(request, "webApp/settings.html",context={'error':'Current Password Incorrect'})
		else:
			u.set_password(request.POST['newPass'])
		u.save()
		return HttpResponseRedirect('/webApp/logout/')
	else:
		return render(request, "webApp/settings.html",context={'error':''})


#Content pages
@login_required()
def courses(request):
	return render(request, "webApp/courses.html",context={'courses' : Course.objects.all()})

@login_required()
def coursesnew(request):
	if request.POST :
		if(len(Course.objects.filter(code=request.POST['code'])) != 0):
			data={
				'professors' : Professor.objects.all(),
				'students' : Student.objects.all(),
				'errors' : 'Course Already Exists'
			}
			return render(request, "webApp/coursesnew.html",context=data)

		profuser = User.objects.get(username=request.POST['professor'])
		prof = Professor.objects.get(user=profuser)

		c = prof.course_set.create(name=request.POST['name'],code=request.POST['code'],
			period=request.POST['group1'],year=request.POST['year'])
		c.save()



		d1 = Deadline(course=c,time=request.POST['time3'],description=request.POST['description3'],
			date=request.POST['date3'])
		d2 = Deadline(course=c,time=request.POST['time4'],description=request.POST['description4'],
			date=request.POST['date4'])
		d1.save()
		d2.save()


		deadl = c.deadline_set.create(time=request.POST['time2'],description=request.POST['description2'],
			date=request.POST['date2'])
		deadl.save()
		feedbck = deadl.feedbackform_set.create()
		feedbck.save()

		desc1 = "How good was teaching of instructor?"
		feedbck.question_set.create(description=desc1)
		desc2 = "Percentage of Classes you attended?"
		feedbck.question_set.create(description=desc2)

		deadl1 = c.deadline_set.create(time=request.POST['time1'],description=request.POST['description1'],
			date=request.POST['date1'])
		deadl1.save();

		feedbck1 = deadl1.feedbackform_set.create()
		feedbck1.save()

		desc3 = "How good was teaching of instructor?"
		feedbck1.question_set.create(description=desc3)
		desc4 = "Percentage of Classes you attended?"
		feedbck1.question_set.create(description=desc4)


		studusernames = request.POST.getlist('student')
		for i in range(len(studusernames)):
			studuser = User.objects.get(username=studusernames[i])
			stud = Student.objects.get(user=studuser)
			c.students.add(stud)

		return HttpResponseRedirect('/webApp/home/')
	else:
		data={
		'professors' : Professor.objects.all(),
		'students' : Student.objects.all(),
		'errors' : ''
		}
		return render(request, "webApp/coursesnew.html",context=data)

@login_required()
def deadlines(request):
	return render(request, "webApp/deadlines.html",context={'deadlines' : Deadline.objects.all()})

@login_required()
def deadlinesnew(request):
	if request.POST :
		corse = Course.objects.filter(code=request.POST['course'])
		deadl = corse[0].deadline_set.create(date=request.POST['date'],time=request.POST['time'],description=request.POST['description'])
		deadl.save()

		return HttpResponseRedirect('/webApp/home/')
	else:
		return render(request, "webApp/deadlinesnew.html",context={'courses' : Course.objects.all()})

@login_required()
def feedback(request):
	data={'forms' : [],
	'feedbacks' : []}
	x=FeedbackForm.objects.all()
	data['forms'] = FeedbackForm.objects.all();
	for i in range(len(x)):
		data[i] = Question.objects.filter(feedbackForm=x[i]);
	data['length'] =  len(x);
	return render(request, "webApp/feedback.html",context=data)


@login_required()
def feedbacknew(request):
	if request.POST :
		corse = Course.objects.filter(code=request.POST['course'])
		deadl = corse[0].deadline_set.create(date=request.POST['date'],time=request.POST['time'],description=request.POST['description'])
		deadl.save()
		feedbck = deadl.feedbackform_set.create()
		feedbck.save()
		noQ = int(request.POST['noQuestions'])
		for i in range(1,noQ+1):
			desc = request.POST['question'+str(i)]
			feedbck.question_set.create(description=desc)



		return HttpResponseRedirect('/webApp/home/')
	else:
		return render(request,  "webApp/feedbacknew.html",context={'courses' : Course.objects.all()})


@login_required()
def studentInitialise(request):
	if request.POST :

		myfile= request.FILES.get('students')
		with open('webApp/students.csv', 'wb+') as destination:
			for chunk in myfile.chunks():
				destination.write(chunk)

		with open('webApp/students.csv', 'rt') as f:
			reader = csv.reader(f)
			count=0

			for row in reader:
				count = count +1
				if count==1 :
					continue
				us = User.objects.filter(username=row[0])
				if len(us)!=0:
					us=us[0]
				else:
					us = User(username=row[0])
				us.first_name=row[3]
				us.last_name=row[4]
				us.is_staff = False
				us.set_password(row[1]);
				us.save()

				newst = Student(user=us,rollno=row[2])
				newst.save();

		return HttpResponseRedirect('/webApp/home/')
	else:
		return render(request,  "webApp/studentInitialise.html",context={'students' : Student.objects.all()})


# API service
from django.views.decorators.csrf import ensure_csrf_cookie
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from django.middleware.csrf import get_token

@ensure_csrf_cookie
@csrf_exempt
def android_data(request):
	if request.method== 'POST':

		json_str=((request.body).decode('utf-8'))
		data = json.loads(json_str)
		user_name = data['username']
		pw = data['password']

		# user_name = request.POST['username']
		# pw = request.POST['password']

		usr = authenticate(username=user_name, password=pw)
		if usr is not None:
			if usr.is_active:
				s = Student.objects.filter(user=usr)
				if len(s) == 1 :
					s[0].token = get_token(request)
					auth_login(request,s[0].user);
					return JsonResponse(
						{
							'status':'yes',
							'csrftoken':request.COOKIES.get('csrftoken'),
							'sessionid':request.session.session_key
						}
						)
	return JsonResponse({'status':'no','csrftoken':request.COOKIES.get('csrftoken'), 'sessionid':request.session.session_key})
