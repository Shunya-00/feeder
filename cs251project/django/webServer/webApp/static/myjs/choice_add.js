var counter = 1;
var limit = 5;

function addInput(){
  if (counter == limit)  {
    alert("You have reached the limit of adding " + counter + " inputs");
  }

  else {
    document.getElementById("subtractButton").setAttribute('class','btn-floating waves-effect waves-light');
    var newdiv = document.createElement('div');
      newdiv.setAttribute('class','input-field');
      newdiv.setAttribute('id','Q'+(counter+1).toString());
    var newlabel = document.createElement('label');
      newlabel.setAttribute('for','question'+(counter+1).toString());
      newlabel.innerHTML = "Question " + (counter+1).toString();
    var newinput = document.createElement('input');
      newinput.setAttribute('required','');
      newinput.setAttribute('aria-required','true');
      newinput.setAttribute('name','question'+(counter+1).toString());
      newinput.setAttribute('id','question'+(counter+1).toString());
      newinput.setAttribute('type','text');
      newinput.setAttribute('class','validate');

    newdiv.appendChild(newlabel);
    newdiv.appendChild(newinput);

    counter++;
    document.getElementById("Questions").appendChild(newdiv);
    document.getElementById("noQ").setAttribute('value',counter);

    if(counter==limit)
      document.getElementById("addButton").setAttribute('class','disabled btn-floating waves-effect waves-light');
   }
}

function subtractInput(){
  if (counter == 1)  {
    alert("You need to have atleast 1 Question");
  }  else {
    document.getElementById("addButton").setAttribute('class','btn-floating waves-effect waves-light');
    document.getElementById("Q"+ counter.toString()).remove();
    counter--;
    document.getElementById("noQ").setAttribute('value',counter);
    if(counter==1)
      document.getElementById("subtractButton").setAttribute('class','disabled btn-floating waves-effect waves-light');
   }
}
