#for basic rendering of html pages
from django.shortcuts import render, render_to_response
from django.template import RequestContext
from django.http import HttpResponse, HttpResponseRedirect

#for authentication login and logout
from django.contrib.auth import login as auth_login
from django.contrib.auth import logout as auth_logout
from django.contrib.auth import authenticate
from django.contrib.auth.decorators import login_required

#models
from django.contrib.auth.models import User
from .models import *

#read student data
import csv
import json

#ins_content
from datetime import date
import datetime
from itertools import chain

#global variables
prof_url='/webApp/login/'
#Admin Settings
def login(request):
	if request.POST :
		user = authenticate(username=request.POST['name'], password=request.POST['pass'])
		if user is not None:
			if user.is_active:
				auth_login(request, user)
				return HttpResponseRedirect('/webApp/admin/home/')
		return render(request, "webApp/admin/login.html",context={'error':'invalid credentials'})
	else:
		if(request.user.is_authenticated):
			return HttpResponseRedirect('/webApp/admin/home/')
		else:
			return render(request, "webApp/admin/login.html",context={'error':''})

@login_required()
def home(request):
	print(request.user.is_superuser)
	if not request.user.is_superuser:
		return HttpResponse("403 Forbidden")
	return render(request, "webApp/admin/home.html")

@login_required()
def logout(request):
	if not request.user.is_superuser:
		return HttpResponse("403 Forbidden")
	data={'name':request.user.username}
	auth_logout(request)
	return render(request, "webApp/admin/logout.html",context=data)

@login_required()
def settings(request):
	if not request.user.is_superuser:
		return HttpResponse("403 Forbidden")
	if request.POST :
		u = User.objects.get(username__exact=request.user.username)
		if not u.check_password(request.POST['currPass']) :
			return render(request, "webApp/admin/settings.html",context={'error':'Current Password Incorrect'})
		else:
			u.set_password(request.POST['newPass'])
		u.save()
		return HttpResponseRedirect('/webApp/admin/logout/')
	else:
		return render(request, "webApp/admin/settings.html",context={'error':''})


#Content pages for Admin
@login_required()
def courses(request):
	if not request.user.is_superuser:
		return HttpResponse("403 Forbidden")
	return render(request, "webApp/admin/courses.html",context={'courses' : Course.objects.all()})

@login_required()
def coursesnew(request):
	if not request.user.is_superuser:
		return HttpResponse("403 Forbidden")
	if request.POST :
		if(len(Course.objects.filter(code=request.POST['code'])) != 0):
			data={
				'professors' : Professor.objects.all(),
				'students' : Student.objects.all(),
				'errors' : 'Course Already Exists'
			}
			return render(request, "webApp/admin/coursesnew.html",context=data)

		profuser = User.objects.get(username=request.POST['professor'])
		prof = Professor.objects.get(user=profuser)

		c = prof.course_set.create(name=request.POST['name'],code=request.POST['code'],
			period=request.POST['group1'],year=request.POST['year'])
		c.save()



		d1 = Deadline(course=c,time=request.POST['time3'],description=request.POST['description3'],
			date=request.POST['date3'])
		d2 = Deadline(course=c,time=request.POST['time4'],description=request.POST['description4'],
			date=request.POST['date4'])
		d1.save()
		d2.save()


		deadl = c.deadline_set.create(time=request.POST['time2'],description=request.POST['description2'],
			date=request.POST['date2'],isFeedback=True)
		deadl.save()
		feedbck = deadl.feedbackform_set.create()
		feedbck.save()

		desc1 = "How good was teaching of instructor?"
		feedbck.question_set.create(description=desc1)
		desc2 = "Percentage of Classes you attended?"
		feedbck.question_set.create(description=desc2)

		deadl1 = c.deadline_set.create(time=request.POST['time1'],description=request.POST['description1'],
			date=request.POST['date1'],isFeedback=True)
		deadl1.save();

		feedbck1 = deadl1.feedbackform_set.create()
		feedbck1.save()

		desc3 = "How good was teaching of instructor?"
		feedbck1.question_set.create(description=desc3)
		desc4 = "Percentage of Classes you attended?"
		feedbck1.question_set.create(description=desc4)


		studusernames = request.POST.getlist('student')
		for i in range(len(studusernames)):
			studuser = User.objects.get(username=studusernames[i])
			stud = Student.objects.get(user=studuser)
			c.students.add(stud)

		return HttpResponseRedirect('/webApp/admin/home/')
	else:
		data={
		'professors' : Professor.objects.all(),
		'students' : Student.objects.all(),
		'errors' : ''
		}
		return render(request, "webApp/admin/coursesnew.html",context=data)

@login_required()
def deadlines(request):
	if not request.user.is_superuser:
		return HttpResponse("403 Forbidden")
	return render(request, "webApp/admin/deadlines.html",context={'deadlines' : Deadline.objects.all()})

@login_required()
def feedback(request):
	if not request.user.is_superuser:
		return HttpResponse("403 Forbidden")
	data={'forms' : [],
	'feedbacks' : []}
	x=FeedbackForm.objects.all()
	data['forms'] = FeedbackForm.objects.all();
	for i in range(len(x)):
		data[i] = Question.objects.filter(feedbackForm=x[i]);
	data['length'] =  len(x);
	return render(request, "webApp/admin/feedback.html",context=data)

#Instructor Settings
def ins_login(request):
	if request.POST :
		usr = User.objects.filter(username=request.POST['username'])
		if len(usr) == 1:
			if usr[0].is_staff and not usr[0].is_superuser:
				user = authenticate(username=request.POST['username'],
					password=request.POST['password'])
				if user is not None:
					if user.is_active:
						auth_login(request, user)
						return HttpResponseRedirect('/webApp/home/')
		return render(request, "webApp/instructor/ins_login.html",
			context={'error':'Invalid Credentials'})
	else:
		print(request.method)
		if(request.user.is_authenticated):
			return HttpResponseRedirect('/webApp/home/')
		else:
			return render(request, "webApp/instructor/ins_login.html",context={'error':''})

def ins_signup(request):
	if request.POST :
		u = User.objects.filter(username=request.POST['username'])
		if len(u) > 0 :
			return render(request, "webApp/instructor/ins_signup.html",
				context={'error':'Username already exits'})

		x= User(username = request.POST['username'],email=request.POST['email'])
		x.set_password(request.POST['password'])
		x.first_name=request.POST['first_name']
		x.last_name=request.POST['last_name']
		x.is_staff=True
		x.save()
		p=Professor(user=x);
		p.save()
		auth_login(request,x)
		return HttpResponseRedirect('/webApp/home/')

	else:
		if(request.user.is_authenticated):
			return HttpResponseRedirect('/webApp/home/')
		else:
			return render(request, "webApp/instructor/ins_signup.html",context={'error':''})

@login_required(login_url=prof_url)
def ins_logout(request):
	if request.user.is_superuser or not request.user.is_staff:
		return HttpResponse("403 Forbidden")
	data={'name':request.user.first_name + " " +request.user.last_name}
	auth_logout(request)
	return render(request, "webApp/instructor/ins_logout.html",context=data)

@login_required(login_url=prof_url)
def ins_settings(request):
	if request.user.is_superuser or not request.user.is_staff:
		return HttpResponse("403 Forbidden")
	if request.POST :
		u = User.objects.get(username__exact=request.user.username)
		if not u.check_password(request.POST['currPass']) :
			return render(request, "webApp/instructor/ins_settings.html",context={'error':'Current Password Incorrect'})
		else:
			u.set_password(request.POST['newPass'])
		u.save()
		return HttpResponseRedirect('/webApp/logout/')
	else:
		return render(request, "webApp/instructor/ins_settings.html",context={'error':''})

@login_required(login_url=prof_url)
def ins_home(request):
	if request.user.is_superuser or not request.user.is_staff:
		return HttpResponse("403 Forbidden")
	return render(request, "webApp/instructor/ins_home.html")


#Content pages for Instructor
@login_required(login_url=prof_url)
def deadlinesnew(request):
	if request.user.is_superuser or not request.user.is_staff:
		return HttpResponse("403 Forbidden")
	if request.POST :
		corse = Course.objects.filter(code=request.POST['course'])
		deadl = corse[0].deadline_set.create(date=request.POST['date'],time=request.POST['time'],description=request.POST['description'])
		deadl.save()

		return HttpResponseRedirect('/webApp/home/')
	else:
		return render(request, "webApp/instructor/deadlinesnew.html",context={'courses' : Course.objects.all()})

@login_required(login_url=prof_url)
def ins_feedback(request):
	if request.user.is_superuser or not request.user.is_staff:
		return HttpResponse("403 Forbidden")
	return render(request, "webApp/instructor/feedback.html",context={'forms':FeedbackForm.objects.all()})

@login_required(login_url=prof_url)
def present_feed(request,form_id):
	if request.user.is_superuser or not request.user.is_staff:
		return HttpResponse("403 Forbidden")
	feed = FeedbackForm.objects.get(id=form_id)

	total_students = len(Student.objects.filter(course=feed.deadline.course))
	questions = Question.objects.filter(feedbackForm=feed)
	detail={}
	total_responses = 0
	for q in questions:
		detail[q.id]=Response.objects.filter(question=q)
	if len(questions) > 0:
		total_responses = len(Response.objects.filter(question=questions[0]))
	return render(request,"webApp/instructor/detailedfeed.html",context={
		'detail':detail,
		'questions':questions,
		'form':feed,
		'total_students':total_students,
		'total_responses':len(list(range(total_responses))),
		'count_responses':list(range(total_responses))
	})

@login_required(login_url=prof_url)
def ques_stats(request,ques_id):
	if request.user.is_superuser or not request.user.is_staff:
		return HttpResponse("403 Forbidden")
	ques = Question.objects.get(id=ques_id)
	res = Response.objects.filter(question=ques)
	return render(request,"webApp/instructor/question_stats.html",context={
		'responses':res,
		'data1':len(res.filter(response=1)),
		'data2':len(res.filter(response=2)),
		'data3':len(res.filter(response=3)),
		'data4':len(res.filter(response=4)),
		'data5':len(res.filter(response=5))
	})

@login_required(login_url=prof_url)
def deadlines_r(request):
	if request.user.is_superuser or not request.user.is_staff:
		return HttpResponse("403 Forbidden")
	gt = Deadline.objects.filter(date__gt= date.today())
	gte = Deadline.objects.filter(date= date.today()).filter(time__gte= datetime.datetime.time(datetime.datetime.now()))
	result = list(chain(gte,gt))
	return render(request, "webApp/instructor/running_deadlines.html",context={'deadlines':result})
@login_required(login_url=prof_url)
def deadlines_f(request):
	if request.user.is_superuser or not request.user.is_staff:
		return HttpResponse("403 Forbidden")
	lt = Deadline.objects.filter(date__lt= date.today())
	lte = Deadline.objects.filter(date= date.today()).filter(time__lt= datetime.datetime.time(datetime.datetime.now()))
	result = list(chain(lte,lt))
	return render(request, "webApp/instructor/finished_deadlines.html",
		context={'deadlines':result})

@login_required(login_url=prof_url)
def feedbacknew(request):
	if request.user.is_superuser or not request.user.is_staff:
		return HttpResponse("403 Forbidden")
	if request.POST :
		corse = Course.objects.filter(code=request.POST['course'])
		deadl = corse[0].deadline_set.create(date=request.POST['date'],time=request.POST['time'],description=request.POST['description'],isFeedback=True)
		deadl.save()
		feedbck = deadl.feedbackform_set.create()
		feedbck.save()
		noQ = int(request.POST['noQuestions'])
		for i in range(1,noQ+1):
			desc = request.POST['question'+str(i)]
			feedbck.question_set.create(description=desc)



		return HttpResponseRedirect('/webApp/home/')
	else:
		return render(request,  "webApp/instructor/feedbacknew.html",context={'courses' : Course.objects.all()})


# API service
from django.views.decorators.csrf import ensure_csrf_cookie
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from django.middleware.csrf import get_token
from django.core import serializers

@csrf_exempt
@ensure_csrf_cookie
def android_data(request):
	if request.method== 'POST':
		json_str=((request.body).decode('utf-8'))
		data = json.loads(json_str)
		user_name = data['username']
		pw = data['password']

		# user_name = request.POST['username']
		# pw = request.POST['password']


		usr = authenticate(username=user_name, password=pw)
		if usr is not None:
			if usr.is_active:
				s = Student.objects.filter(user=usr)
				if len(s) == 1 :
					s[0].token = get_token(request)
					auth_login(request,s[0].user);
					if not request.session.session_key:
						request.session.create()

					return JsonResponse(
						{
							'status':'yes',
							'sessionid':request.session.session_key,
							# 'csrftoken':request.COOKIES.get('csrftoken'),
						}
						)

	return JsonResponse({'status':'no',	'sessionid':request.session.session_key})

@login_required()
def studentInitialise(request):
	if not request.user.is_superuser:
		return HttpResponse("403 Forbidden")
	if request.POST :

		myfile= request.FILES.get('students')
		with open('webApp/students.csv', 'wb+') as destination:
			for chunk in myfile.chunks():
				destination.write(chunk)

		with open('webApp/students.csv', 'rt') as f:
			reader = csv.reader(f)
			count=0

			for row in reader:
				count = count +1
				if count==1 :
					continue
				us = User.objects.filter(username=row[0])
				if len(us)!=0:
					us=us[0]
				else:
					us = User(username=row[0])
				us.first_name=row[3]
				us.last_name=row[4]
				us.is_staff = False
				us.set_password(row[1]);
				us.save()

				newst = Student(user=us,rollno=row[2])
				newst.save();

		return HttpResponseRedirect('/webApp/admin/home/')
	else:
		return render(request,  "webApp/admin/studentInitialise.html",context={'students' : Student.objects.all()})

def socialsignup(request):
	if request.POST:
		u=User.objects.filter(username=request.POST['id'])
		if len(u) == 0:
			new_prof=User(username=request.POST['id'])
			new_prof.first_name = request.POST['first_name']
			new_prof.last_name = request.POST['last_name']
			new_prof.email = request.POST['email']
			new_prof.is_staff=True;
			new_prof.set_password(request.POST['email']);
			new_prof.save()
			p=Professor(user=new_prof)
			p.save()
		else:
			new_prof = u[0]
			if len(Professor.objects.filter(user=new_prof)) != 1 :
				return HttpResponse("403 forbidden")
		auth_login(request,new_prof);
		return HttpResponseRedirect('/webApp/home/')

@ensure_csrf_cookie
@csrf_exempt
def android_feedbacklist(request):
	if request.method == 'POST':
		if request.user.is_authenticated():
			s = Student.objects.filter(user=request.user)
			if len(s) == 1 :
				courses = Course.objects.filter(students__in=s)
				deadline = Deadline.objects.filter(course__in=courses)
				x = serializers.serialize('json',deadline)
				return HttpResponse(x)
	return JsonResponse({'status':'no', 'sessionid':request.session.session_key})


@ensure_csrf_cookie
@csrf_exempt
def android_signout(request):
	if request.method == 'POST':
		if request.user.is_authenticated():
			logout(request)
	return JsonResponse({'status':'no', 'sessionid':request.session.session_key})
