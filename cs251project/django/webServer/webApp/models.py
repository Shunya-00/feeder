from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from datetime import date
from datetime import time
import calendar

MaxNameLength = 30;

default_deadlines = {
    'Spring':[date.today().replace(day=28, month=2) ,date.today().replace(day=15, month=4) ],
    'Summer':[date.today().replace(day=1, month=6) ,date.today().replace(day=15, month=7)],
    'Autumn':[date.today().replace(day=6, month=9) ,date.today().replace(day=18, month=11)]
}

class Professor(models.Model):

    user = models.OneToOneField(User, on_delete=models.CASCADE)

    def __str__(self):
        return "%s: %s %s" %(self.usernameer.username, self.user.first_name, self.user.last_name)

class Student(models.Model):

    user = models.OneToOneField(User, on_delete=models.CASCADE)
    rollno = models.CharField(max_length = 15, primary_key=True)
    token = models.CharField(max_length = 64, default="0")

    def __str__(self):
        return "%s: %s %s" %(self.rollno,self.user.first_name, self.user.last_name)

    class Meta:
        ordering = ('rollno',)

class Course(models.Model):

    code = models.CharField(max_length = 10, primary_key=True)
    name = models.CharField(max_length = 50)
    period = models.CharField(max_length = 20,default='Summer')
    year = models.CharField(max_length = 20,default='2016')

    professor = models.ForeignKey(Professor)
    students = models.ManyToManyField(Student)

    def __str__(self):
        return "%s: %s" %(self.code, self.name)

    class Meta:
        ordering = ('code','name')

class Deadline(models.Model):

    date = models.DateField()
    time = models.TimeField()
    course = models.ForeignKey(Course)
    description = models.TextField(max_length=50)
    isFeedback = models.BooleanField(default=False)

    def __str__(self):
        return "%s" %(self.description)

class FeedbackForm(models.Model):
    deadline = models.ForeignKey(Deadline)
    student = models.ManyToManyField(Student)

class Question(models.Model):
    description = models.TextField(max_length=200)
    feedbackForm = models.ForeignKey(FeedbackForm)

class Response(models.Model):
    question = models.ForeignKey(Question)
    response = models.SmallIntegerField()
