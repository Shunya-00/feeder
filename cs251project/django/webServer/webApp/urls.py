from django.conf.urls import url

from . import views

app_name = 'webApp'
urlpatterns = [

    #Admin
    url(r'admin/login/', views.login, name='login'),
	url(r'admin/logout/', views.logout, name='logout'),

    url(r'admin/settings/', views.settings, name='settings'),
    url(r'admin/home/', views.home, name='home'),
    url(r'admin/studentInitialise/', views.studentInitialise, name='studentInitialise'),

    url(r'admin/courses/', views.courses, name='courses'),
	url(r'admin/deadlines/', views.deadlines, name='deadlines'),
    url(r'admin/feedback/', views.feedback, name='feedback'),
    url(r'admin/coursesnew/', views.coursesnew, name='coursesnew'),

    #Instructor
    url(r'deadlinesnew/', views.deadlinesnew, name='ins_deadlinesnew'),
	url(r'feedbacknew/', views.feedbacknew, name='ins_feedbacknew'),
    url(r'deadlinesrunning/', views.deadlines_r, name='deadlines_r'),
    url(r'deadlinesfinished/', views.deadlines_f, name='deadlines_f'),
    url(r'feedback/', views.ins_feedback, name='ins_feedback'),
    url(r'forms/(?P<form_id>[0-9]+)/', views.present_feed, name='present_feed'),
    url(r'forms/question/(?P<ques_id>[0-9]+)/', views.ques_stats, name='ques_stats'),

    url(r'signup/', views.ins_signup, name='ins_signup'),
    url(r'login/', views.ins_login, name='ins_login'),
    url(r'logout/', views.ins_logout, name='ins_logout'),
    url(r'signupsocial/', views.socialsignup, name='socialsignup'),

    url(r'settings/', views.ins_settings, name='ins_settings'),
    url(r'home/', views.ins_home, name='ins_home'),

    #Student
    url(r'api/signin/', views.android_data, name='signin'),
    url(r'api/signout/', views.android_signout, name='signout'),
    url(r'api/feedbacklist/', views.android_feedbacklist, name='feedbackList'),
]
