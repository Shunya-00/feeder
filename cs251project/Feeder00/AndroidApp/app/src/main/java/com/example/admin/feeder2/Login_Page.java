package com.example.admin.feeder2;

import android.content.Intent;
import android.graphics.Bitmap;

import java.sql.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CaldroidListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.ArrayList;

public class Login_Page extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    HttpURLConnection urlConnection3;
    TextView calendarinfo;
    Map<String, JSONObject> allmydata;
    Map<Date, ArrayList<String> > assignmentlist = new HashMap<>();
    Map<Date, ArrayList<String> > feedbackforms = new HashMap<>();
    Map<Date, List<String> > datetoCourse = new HashMap<>();
    Map<Date, Drawable> colormap;
    ListView mylist;
    CaldroidFragment caldroidFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        colormap = new HashMap<>();

        String s;
//        Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.ass);
        //genarateData();

        setContentView(R.layout.activity_login__page);
//        mylist = (ListView) findViewById(R.id.listview);
        calendarinfo = (TextView) findViewById(R.id.Calendarinfo);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Calendar");
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setVisibility(View.GONE);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        caldroidFragment = new CaldroidFragment();
        FragmentTransaction t = getSupportFragmentManager().beginTransaction();
        t.replace(R.id.calenderview, caldroidFragment);
        t.commit();

        final Date Lastdate = new Date(System.currentTimeMillis());
        caldroidFragment.setTextColorForDate(R.color.black, Lastdate);

        CaldroidListener listener = new CaldroidListener() {
            Date last = new Date(System.currentTimeMillis());
            @Override
            public final void onSelectDate(Date date, View view) {
                caldroidFragment.clearTextColorForDate(last);
                caldroidFragment.setTextColorForDate(R.color.caldroid_light_red, date);
                last = date;
                caldroidFragment.refreshView();
                    if(assignmentlist.containsKey(date)){
                        calendarinfo.setVisibility(View.VISIBLE);
                        String info = "";
                        for (int i=0; i< assignmentlist.get(date).size(); i++){
                            //Log.e()
                            info = info + assignmentlist.get(date).get(i) + System.lineSeparator() + System.lineSeparator() + System.lineSeparator() ;
                            Log.e("Myapp", info);
                            calendarinfo.setText(info);
                            calendarinfo.setVisibility(View.VISIBLE);
                        }
                    }
                    else calendarinfo.setVisibility(View.INVISIBLE);
                }
        };

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        //client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();

        GetText3 g3 = new GetText3();
        g3.execute();
        caldroidFragment.setCaldroidListener(listener);
    }

    public class GetText3 extends AsyncTask<Void, Void, Void> {
        // Get user defined values

        @Override
        protected void onPreExecute(){
            //dialog = ProgressDialog.show(MainFeedActivity.this, null, "Posting...");
        }


        @Override
        protected Void doInBackground(Void Params[]) {

            //Log.e("Myapp", "--------------------------- Password or Username not filled");

            // Create data variable for sent values to server
            String data = "heyy";
            try {
                JSONObject jsonObject2 = new JSONObject();
                //jsonObject2.put("csrftoken", SaveSharedPreferences.getCookie(getApplicationContext()));
                Log.e("myapp", SaveSharedPreferences.getCookie(getApplicationContext()));
                jsonObject2.put("username", SaveSharedPreferences.getUserName(getApplicationContext()));
                data = jsonObject2.toString();
                Log.e("Myapp", "--------------------------- No error in json: " + data);
            } catch (Exception ex) {
                Log.e("Myapp", "---------------------------Error in Creating Json");
                return null;
            }
            int tries = 0;
            while (tries != 20){
                try {   tries++;
                    Log.e("Myapp", "--------------------------- Connecting: ");
                    //URL url = new URL("http://10.0.2.2:8000/webApp/api/");
                    URL url = new URL(Startup.MainURL + "webApp/api/feedbacklist/");
                    urlConnection3 = (HttpURLConnection) url.openConnection();
                    urlConnection3.setRequestMethod("POST");
                    //urlConnection3.setRequestProperty("Cookie", );
                    String cook = SaveSharedPreferences.getCookie(getApplicationContext());

                    Log.e("Myapp", "--------------------------- Connected once: ");
                    String whatam = SaveSharedPreferences.getUserName(getApplicationContext());
                    whatam = "sessionid=" + whatam;
                    Log.e("MyApp", "------------------------------- Cookie is   " + whatam);
                    urlConnection3.setRequestProperty("Cookie", whatam);
                    urlConnection3.setDoOutput(true);
                    urlConnection3.setDoInput(true);
                    urlConnection3.connect();
                    Log.e("Myapp", "--------------------------- Connected twice: ");

                    try {
                        OutputStreamWriter wrt = new OutputStreamWriter(urlConnection3.getOutputStream());
                        wrt.write(data);
                        wrt.flush();
                        wrt.close();
                    } catch (Exception gh) {
                        Log.e("Myapp", "--------------------------- Error in Writing to Server");
                        return null;
                    }
                    // Get the server response
                    String text="";
                    try {
                        if (urlConnection3.getResponseCode() == HttpURLConnection.HTTP_OK) {

                            BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection3.getInputStream()));
                            StringBuffer sb = new StringBuffer("");
                            String line = "";
                            // Read Server Response
                            while ((line = in.readLine()) != null) {
                                sb.append(line);
                            }
                            text = sb.toString();
                            try {
                                in.close();
                            } catch (Exception f) {
                                f.printStackTrace();
                            }
                        } else {
                            return null;
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        return null;
                    }
                    JSONArray eventsdates = new JSONArray(text);
                    datetoCourse.clear();
                    colormap.clear();
                    assignmentlist.clear();
                    feedbackforms.clear();
                    Log.e("Myapp", "--------------------------- Auth:  " + eventsdates.toString());
                    //              JSONArray arrofdeadlines = jsonObject2.getJSONArray("deadlines");
                    // datetoCourse.put()
                    JSONObject json;
                    JSONObject internal;
                    Bitmap ab = BitmapFactory.decodeResource(getResources(), R.drawable.assff);
                    Date daybeingcoloured;
                    String str[] = new String[10];
                    for(int i=0;i<10;i++)
                        str[i]="";
                    int coursenum;
                    int numcourses = 0;
                    Log.e("Myapp", "--------------------------- Entering For Loop");

                    for (int h = 0; h < eventsdates.length(); h++) {
                        coursenum = 0;
                        json = eventsdates.getJSONObject(h);
                        internal = json.getJSONObject("fields");
                        String day = internal.getString("date");
                        String description = internal.getString("description");
                        String time = internal.getString("time");
                        Boolean feedbackornot = internal.getBoolean("isFeedback");
                        Integer pk = json.getInt("pk");
                        Log.e("Myapp", "ABCDF");
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                        daybeingcoloured = sdf.parse(day);
                        String course = internal.getString("course");
                        ArrayList<String> query;
                        ArrayList<String> formquery;
                        if(feedbackornot){
                            String form = pk.toString() + "Course: " + course + "  Deadline: " + time;
                            if(feedbackforms.containsKey(daybeingcoloured)){
                                formquery = assignmentlist.get(daybeingcoloured);
                            }
                            else formquery = new ArrayList<>();
                            formquery.add(form);
                            feedbackforms.put(daybeingcoloured, formquery);
                        }


                        if(assignmentlist.containsKey(daybeingcoloured)){
                                query = assignmentlist.get(daybeingcoloured);
                        }
                        else query = new ArrayList<>();
                        String s2 = course + "   " + "Endtime:  " + time + System.lineSeparator()+"Work:  " + description ;
                        query.add(s2);
                        assignmentlist.put(daybeingcoloured, query);
                        //expandableListAdapter.notifyDataSetChanged();
                        Log.e("yapp","-----------------------"+ s2);
                        Log.e("yapp","-----------------------"+ query);
                        int z = 0;
                        for (z = 0; z < numcourses; z++) {
                            if (str[z].equals(course)) {
                                //                                z = Integer.toString(l);
                                coursenum = z;
                                break;
                            }
                        }
                        Log.e("Myapp", "--------------------------- course " + course + coursenum);
                        if (z == numcourses) {
                            str[numcourses] = course;
                            coursenum = numcourses;
                            numcourses++;
                        }
                        Log.e("Myapp", "--------------------------- course " + course + str[0] + str[1]);
                        Log.e("Myapp", "--------------------------- course " + course + str[0] + str[1]);
                        //                    String z = "sjah";
                        //                    for(int l=0; l<numcourses; l++ ){
                        //                        if(course.equals(str[l])){ z = Integer.toString(l); coursenum = l;}
                        //                    }
                        Log.e("Myapp", "--------------------------- coursenum is " + z);
                        List<String> x = new ArrayList<>();
                        if (datetoCourse.containsKey(daybeingcoloured)) {
                            x = datetoCourse.get(daybeingcoloured);
                        }
                        Log.e("Myapp", "--------------------------- coursenum is " + str[coursenum]);
                        x.add(str[coursenum]);
                        Log.e("Myapp", "--------------------------- coursenum is " + z);
                        datetoCourse.put(daybeingcoloured, x);
                        List<String> y= new ArrayList<>();
                        y = datetoCourse.get(daybeingcoloured);
                        Log.e("Myapp", "--------------------------- coursenum is " + z);
                        if (y.contains(str[0]) && y.contains(str[1]) && y.contains(str[2]) && y.contains(str[3])) {
                            ab = BitmapFactory.decodeResource(getResources(), R.drawable.a1234);
                        }            //1
                        else if (y.contains(str[0]) && y.contains(str[1]) && y.contains(str[2]) && !y.contains(str[3])) {
                            ab = BitmapFactory.decodeResource(getResources(), R.drawable.a123);
                        }       //2
                        else if (y.contains(str[0]) && y.contains(str[1]) && !y.contains(str[2]) && y.contains(str[3])) {
                            ab = BitmapFactory.decodeResource(getResources(), R.drawable.a124);
                        }        //3
                        else if (y.contains(str[0]) && !y.contains(str[1]) && y.contains(str[2]) && y.contains(str[3])) {
                            ab = BitmapFactory.decodeResource(getResources(), R.drawable.a134);
                        }        //4
                        else if (!y.contains(str[0]) && y.contains(str[1]) && y.contains(str[2]) && y.contains(str[3])) {
                            ab = BitmapFactory.decodeResource(getResources(), R.drawable.a234);
                        }        //5
                        else if (y.contains(str[0]) && y.contains(str[1]) && !y.contains(str[2]) && !y.contains(str[3])) {
                            ab = BitmapFactory.decodeResource(getResources(), R.drawable.a12);
                        }       //6
                        else if (y.contains(str[0]) && !y.contains(str[1]) && y.contains(str[2]) && !y.contains(str[3])) {
                            ab = BitmapFactory.decodeResource(getResources(), R.drawable.a13);
                        }       //7
                        else if (!y.contains(str[0]) && y.contains(str[1]) && y.contains(str[2]) && !y.contains(str[3])) {
                            ab = BitmapFactory.decodeResource(getResources(), R.drawable.a23);
                        }       //8
                        else if (y.contains(str[0]) && !y.contains(str[1]) && !y.contains(str[2]) && y.contains(str[3])) {
                            ab = BitmapFactory.decodeResource(getResources(), R.drawable.a14);
                        }       //9
                        else if (!y.contains(str[0]) && y.contains(str[1]) && !y.contains(str[2]) && y.contains(str[3])) {
                            ab = BitmapFactory.decodeResource(getResources(), R.drawable.a24);
                        }   //10
                        else if (!y.contains(str[0]) && !y.contains(str[1]) && y.contains(str[2]) && y.contains(str[3])) {
                            ab = BitmapFactory.decodeResource(getResources(), R.drawable.a34);
                        }   //11
                        else if (y.contains(str[0]) && !y.contains(str[1]) && !y.contains(str[2]) && !y.contains(str[3])) {
                            ab = BitmapFactory.decodeResource(getResources(), R.drawable.a1);
                        }     //12
                        else if (!y.contains(str[0]) && y.contains(str[1]) && !y.contains(str[2]) && !y.contains(str[3])) {
                            ab = BitmapFactory.decodeResource(getResources(), R.drawable.a2);
                        }     //13
                        else if (!y.contains(str[0]) && !y.contains(str[1]) && y.contains(str[2]) && !y.contains(str[3])) {
                            ab = BitmapFactory.decodeResource(getResources(), R.drawable.a3);
                        }     //14
                        else if (!y.contains(str[0]) && !y.contains(str[1]) && !y.contains(str[2]) && y.contains(str[3])) {
                            ab = BitmapFactory.decodeResource(getResources(), R.drawable.a4);
                        }     //15

                        Drawable d = new BitmapDrawable(getResources(), ab);
                        colormap.put(daybeingcoloured, d);
                        caldroidFragment.setBackgroundDrawableForDates(colormap);
                        tries=20;
                        //x.add(course);
                        //datetoCourse.remove(daybeingcoloured);
                        //datetoCourse.put(daybeingcoloured, x);
                        //Parse String to Date
                    }
                    //String mycookie = urlConnection3.getHeaderField("Set-Cookie");
                    //SaveSharedPreferences.setCookie(getApplicationContext(), mycookie);
                    //                /Log.e("Myapp", "--------------------------- Auth:  " + arrofdeadlines.toString());
                    //String newcookie = jsonObject2.getString("Set-Cookie");
                    urlConnection3.disconnect();
                } catch (Exception e) {
                    // TODO Auto-generated catch block

                    e.printStackTrace();
                    Log.e("Myapp", "--------------------------- Could not Connect");
                }

        }
               /*       runOnUiThread(new Runnable() {
                public void run(){
                    Toast toast = Toast.makeText(SignIn.this, "Check Network Connection..!!", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.TOP,10,25);
                    toast.show();
                }
            });*/
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            caldroidFragment.setBackgroundDrawableForDates(colormap);
            caldroidFragment.refreshView();

            //expandableListAdapter=new ExpandableListAdapter(getApplicationContext(), sending);
            //expandableListView.setAdapter(expandableListAdapter);
        }
    }
    
    
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        if (id == R.id.nav_Courses) {
            // Handle the camera action
            Intent intent = new Intent(Login_Page.this, Courses.class);
            startActivity(intent);
            return true;
        } else if (id == R.id.nav_Feedback) {
            Intent intent = new Intent(Login_Page.this, Feedbackform.class);
            startActivity(intent);
            return true;
        } else if (id == R.id.nav_Calendar) {
            return true;
        } else if (id == R.id.nav_Assignments) {
            Intent intent = new Intent(Login_Page.this, Assignments.class);
            startActivity(intent);
            return true;
        } else if (id == R.id.nav_Logout) {
            SaveSharedPreferences.clearUserName(getApplicationContext());
            Intent intent = new Intent(Login_Page.this, SignIn.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            return true;
        }
        return true;
    }
}


    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    /*public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("Login_Page Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        AppIndex.AppIndexApi.start(client, getIndexApiAction());
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(client, getIndexApiAction());
        client.disconnect();
    }
}*/