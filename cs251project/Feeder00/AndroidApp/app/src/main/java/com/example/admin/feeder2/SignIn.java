package com.example.admin.feeder2;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

public class SignIn extends AppCompatActivity{

    String userna;
    String password;
    EditText txtUsername;
    EditText txtPassword;
    Button Login;
    ImageButton theye;
    HttpURLConnection urlConnection = null;
    boolean hidden = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_sign_in);

        Login = (Button) findViewById(R.id.Login);
        theye = (ImageButton) findViewById(R.id.makePasswordVisible);
        txtUsername = (EditText) findViewById(R.id.txtUsername);
        txtPassword = (EditText) findViewById(R.id.txtPassword);

        //if(!hidden) {txtPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());}
        //else {txtPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());}
        //Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //    toolbar.setTitle("Welcome to Our App: Feeder");
        //
        Login.requestFocus();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(txtUsername, InputMethodManager.SHOW_IMPLICIT);

        theye.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(hidden) {txtPassword.setTransformationMethod(PasswordTransformationMethod.getInstance()); hidden = false;}
                else {txtPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance()); hidden = true;}
                return;
            }
        });

        Login.setOnClickListener(new View.OnClickListener() {

        @Override
        public void onClick(View v) {
                //spinner.setVisibility(View.VISIBLE);
                Log.e("Myapp", "--------------------------- Login button was clicked");

                userna = txtUsername.getText().toString();
                password = txtPassword.getText().toString();

                try {
                GetText g = new GetText();
                g.execute();
                } catch (Exception eee) {
                Log.e("Myapp", "--------------------------- ");
                }
            }
        });
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        //client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
        }

public class GetText extends AsyncTask {
    // Get user defined values

    @Override
    protected Object doInBackground(Object[] params) {

        if (userna.length() == 0) {
            runOnUiThread(new Runnable() {
                public void run(){
                    Toast toast = Toast.makeText(SignIn.this, "Please Fill in Username..!!", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.TOP,10,25);
                    toast.show();
                }
            });

            Log.e("Myapp", "--------------------------- Password or Username not filled");
            return null;
        } else if (password.length() == 0) {
            runOnUiThread(new Runnable() {
                public void run(){
                    Toast toast = Toast.makeText(SignIn.this, "Please Fill in Password..!!", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.TOP,10,25);
                    toast.show();
                }
            });
            Log.e("Myapp", "--------------------------- Password or Username not filled");
            return null;
        }
        // Create data variable for sent values to server
        else {
            String data = "heyy";

            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("username", userna);
                jsonObject.put("password", password);
                //jsonObject.put("csrftoken", SaveSharedPreferences.getUserName(getApplicationContext()));
                data = jsonObject.toString();
                Log.e("Myapp", "--------------------------- No error in json: " + data);
            } catch (Exception ex) {
                Log.e("Myapp", "---------------------------Error in Creating Json");
                return null;
            }

            // Send data
            // URL url = new URL("http://10.0.1.7:8000/webApp/api/);
            //String ur = url.toString();

            try {
                Log.e("Myapp", "--------------------------- Connecting: ");
                //URL url = new URL("http://10.0.2.2:8000/webApp/api/");
                URL url = new URL(Startup.MainURL+"webApp/api/signin/");
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("POST");
               // urlConnection.setRequestProperty("Cookie", urlConnection.getHeaderField("Set-Cookie"));
                urlConnection.setDoOutput(true);
                urlConnection.setDoInput(true);
                urlConnection.connect();
                Log.e("Myapp", "--------------------------- Connected: 1");

                try {
                    Log.e("Myapp", "--------------------------- Connected: 2");
                    OutputStreamWriter wrt2 = new OutputStreamWriter(urlConnection.getOutputStream());
                    Log.e("Myapp", "--------------------------- Connected: 3");
                    wrt2.write(data);
                    Log.e("Myapp", "--------------------------- Connected: 4");
                    wrt2.flush();
                    Log.e("Myapp", "--------------------------- Connected: 5");
                    wrt2.close();
                } catch (Exception gh) {
                    Log.e("Myapp", "--------------------------- Error in Writing to Server");
                    return null;
                }
                // Get the server response
                String text = "abc";
                try {
                    if (urlConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                        Log.e("Myapp", "--------------------------- Data is :   " + data);
                        BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                        Log.e("Myapp", "--------------------------- Buffered reader is working");
                        StringBuffer sb = new StringBuffer("");
                        Log.e("Myapp", "--------------------------- Buffered reader is working");
                        String line = "";
                        Log.e("Myapp", "--------------------------- Line:  " + line);
                        // Read Server Response
                        while ((line = in.readLine()) != null) {
                            // Append server response in string
                            sb.append(line);
                        }
                        text = sb.toString();
                        Log.e("Myapp", "--------------------------- Line:  " + text);
                        try {
                            in.close();
                        } catch (Exception f) {
                            f.printStackTrace();
                            Log.e("Myapp", "--------------------------- Error in Closing Reader");
                        }
                    } else {
                        Log.e("Myapp", "--------------------------- Error from Server & response = " + urlConnection.getResponseCode());
                        return null;
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                    Log.e("Myapp", "--------------------------- Done with reading input from Server:   " + text);
                    return null;
                }
                JSONObject jsonObject2 = new JSONObject(text);
                String authentification = jsonObject2.getString("status");
                Log.e("Myapp", "--------------------------- Auth:  " + authentification);
                String efgh = jsonObject2.getString("sessionid");
                SaveSharedPreferences.setUserName(getApplicationContext(), efgh);
                Log.e("MyApp:", "---------------------- sessionid is:" + efgh);
                String mycookie = urlConnection.getHeaderField("Set-Cookie");
                if (authentification.equals("yes")) {
                    //String ssid = jsonObject2.getString("sessionid");
                    SaveSharedPreferences.setCookie(getApplicationContext(), mycookie);
                    Log.e("Myapp", "--------------------------- Authentification Successful");
                    //SaveSharedPreferences.setUserName(getApplicationContext(), userna);
                    Intent myIntent = new Intent(SignIn.this, Login_Page.class);
                    startActivity(myIntent);
                    finish();
                } else {
                    Log.e("Myapp", "--------------------------- Authentification Unsuccessful");
                    runOnUiThread(new Runnable() {
                        public void run(){
                            Toast toast = Toast.makeText(SignIn.this, "Invalid Credentials..!!", Toast.LENGTH_LONG);
                            toast.setGravity(Gravity.TOP,10,25);
                            toast.show();
                        }
                    });
                    return null;
                }
                urlConnection.disconnect();
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                Log.e("Myapp", "--------------------------- Could not Connect");
                runOnUiThread(new Runnable() {
                    public void run(){
                        Toast toast = Toast.makeText(SignIn.this, "Check Network Connection..!!", Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.TOP,10,25);
                        toast.show();
                    }
                });
                return null;
                }
            return null;
            }
        }
    }
}
