package com.example.admin.feeder2;

/**
 * Created by ADMIN on 11/4/2016.
 */

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

/**
 * Created by ADMIN on 11/2/2016.
 */

public class SaveSharedPreferences {

    static final String PREF_USER_NAME = "username";
    static final String COOKIE = "setcookie";
    static SharedPreferences getSharedPreferences(Context ctx) {
        return PreferenceManager.getDefaultSharedPreferences(ctx);
    }


    public static void setUserName(Context ctx, String userName)
    {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(PREF_USER_NAME, userName);
        editor.commit();
    }

    public static void clearUserName(Context ctx)
    {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.clear();
        editor.commit();
    }

    public static String getUserName(Context ctx)
    {
        return getSharedPreferences(ctx).getString(PREF_USER_NAME, "");
    }

    public static String getCookie(Context ctx)
    {
        return getSharedPreferences(ctx).getString(COOKIE, "");
    }

    public static void clearCookie(Context ctx)
    {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.clear();
        editor.commit();
    }

    public static void setCookie(Context ctx, String cookie)
    {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(COOKIE, cookie);
        editor.commit();
    }

}

