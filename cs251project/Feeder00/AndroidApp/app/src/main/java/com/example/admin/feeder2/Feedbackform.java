package com.example.admin.feeder2;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Feedbackform extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    Map<Date, ArrayList<String> > feedbackforms = new HashMap<>();


        Button ff1;
        Button ff2;
        Button ff3;
        LinearLayout buttons;
        HttpURLConnection urlConnection3;
    HttpURLConnection urlConnection;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedbackform);
//        ff1 = (Button) findViewById(R.id.ff1);
//        ff2 = (Button) findViewById(R.id.ff2);
//        ff3 = (Button) findViewById(R.id.ff3);
//        ff1.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                buttons.setVisibility(View.GONE);
//            }
//        });
//
//        ff2.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                buttons.setVisibility(View.GONE);
//
//            }
//        });
//
//        ff3.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                buttons.setVisibility(View.GONE);
//
//            }
//        });

        GetText5 g5 = new GetText5();
        g5.execute();
        /*radio1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
                View radioButton = radioGroup.findViewById(i);
                int index = radioGroup.indexOfChild(radioButton);
            }
        });*/

        
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Feedbackform");
        //setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Feedbackform.this, Login_Page.class);
                startActivity(intent);
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        if (id == R.id.nav_Courses) {
            Intent intent = new Intent(Feedbackform.this, Courses.class);
            startActivity(intent);
            return true;
        } else if (id == R.id.nav_Feedback) {
            return true;
        } else if (id == R.id.nav_Calendar) {
            Intent intent = new Intent(Feedbackform.this, Login_Page.class);
            startActivity(intent);
            return true;
        } else if (id == R.id.nav_Assignments) {
            Intent intent = new Intent(Feedbackform.this, Assignments.class);
            startActivity(intent);
            return true;
        } else if (id == R.id.nav_Logout) {
            SaveSharedPreferences.clearUserName(getApplicationContext());
            Intent intent = new Intent(Feedbackform.this, SignIn.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            return true;
        }
        return true;
    }


public class GetText4 extends AsyncTask {
    // Get user defined values
    @Override
    protected Object doInBackground(Object[] params) {

        String data = "heyy";
            try {
                JSONObject jsonObject = new JSONObject();
               // jsonObject.put("pk:", pk );
               // jsonObject.put("password", password);
                //jsonObject.put("csrftoken", SaveSharedPreferences.getUserName(getApplicationContext()));
                data = jsonObject.toString();
                Log.e("Myapp", "--------------------------- No error in json: " + data);
            } catch (Exception ex) {
                Log.e("Myapp", "---------------------------Error in Creating Json");
                return null;
            }

            // Send data
            // URL url = new URL("http://10.0.1.7:8000/webApp/api/);
            //String ur = url.toString();

            try {
                Log.e("Myapp", "--------------------------- Connecting: ");
                //URL url = new URL("http://10.0.2.2:8000/webApp/api/");
                URL url = new URL(Startup.MainURL+"webApp/api/signin/");
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("POST");
                // urlConnection.setRequestProperty("Cookie", urlConnection.getHeaderField("Set-Cookie"));
                urlConnection.setDoOutput(true);
                urlConnection.setDoInput(true);
                urlConnection.connect();
                Log.e("Myapp", "--------------------------- Connected: 1");

                try {
                    Log.e("Myapp", "--------------------------- Connected: 2");
                    OutputStreamWriter wrt2 = new OutputStreamWriter(urlConnection.getOutputStream());
                    Log.e("Myapp", "--------------------------- Connected: 3");
                    wrt2.write(data);
                    Log.e("Myapp", "--------------------------- Connected: 4");
                    wrt2.flush();
                    Log.e("Myapp", "--------------------------- Connected: 5");
                    wrt2.close();
                } catch (Exception gh) {
                    Log.e("Myapp", "--------------------------- Error in Writing to Server");
                    return null;
                }
                // Get the server response
                String text = "abc";
                try {
                    if (urlConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                        Log.e("Myapp", "--------------------------- Data is :   " + data);
                        BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                        Log.e("Myapp", "--------------------------- Buffered reader is working");
                        StringBuffer sb = new StringBuffer("");
                        Log.e("Myapp", "--------------------------- Buffered reader is working");
                        String line = "";
                        Log.e("Myapp", "--------------------------- Line:  " + line);
                        // Read Server Response
                        while ((line = in.readLine()) != null) {
                            // Append server response in string
                            sb.append(line);
                        }
                        text = sb.toString();
                        Log.e("Myapp", "--------------------------- Line:  " + text);
                        try {
                            in.close();
                        } catch (Exception f) {
                            f.printStackTrace();
                            Log.e("Myapp", "--------------------------- Error in Closing Reader");
                        }
                    } else {
                        Log.e("Myapp", "--------------------------- Error from Server & response = " + urlConnection.getResponseCode());
                        return null;
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                    Log.e("Myapp", "--------------------------- Done with reading input from Server:   " + text);
                    return null;
                }
                JSONObject jsonObject2 = new JSONObject(text);
                String authentification = jsonObject2.getString("status");
                Log.e("Myapp", "--------------------------- Auth:  " + authentification);
                String efgh = jsonObject2.getString("sessionid");
                SaveSharedPreferences.setUserName(getApplicationContext(), efgh);
                Log.e("MyApp:", "---------------------- sessionid is:" + efgh);
                String mycookie = urlConnection.getHeaderField("Set-Cookie");
                urlConnection.disconnect();
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                Log.e("Myapp", "--------------------------- Could not Connect");
                return null;
            }
            return null;
        }
    }


    public class GetText5 extends AsyncTask<Void, Void, Void> {
    // Get user defined values

    @Override
    protected void onPreExecute(){
        //dialog = ProgressDialog.show(MainFeedActivity.this, null, "Posting...");
        Log.e("Myapp", "--------------------------- PRE CONNECTIONS: ");

    }


    @Override
    protected Void doInBackground(Void Params[]) {

        //Log.e("Myapp", "--------------------------- Password or Username not filled");

        // Create data variable for sent values to server
        String data = "heyy";
        try {
            JSONObject jsonObject2 = new JSONObject();
            jsonObject2.put("csrftoken", SaveSharedPreferences.getCookie(getApplicationContext()));
            Log.e("myapp", SaveSharedPreferences.getCookie(getApplicationContext()));
            jsonObject2.put("username", SaveSharedPreferences.getUserName(getApplicationContext()));
            data = jsonObject2.toString();
            Log.e("Myapp", "--------------------------- No error in json: " + data);
        } catch (Exception ex) {
            Log.e("Myapp", "---------------------------Error in Creating Json");
            return null;
        }
        int tries = 0;
        while (tries != 20){
            try {   tries++;
                Log.e("Myapp", "--------------------------- Connecting: ");
                //URL url = new URL("http://10.0.2.2:8000/webApp/api/");
                URL url = new URL(Startup.MainURL + "webApp/api/feedbacklist/");
                urlConnection3 = (HttpURLConnection) url.openConnection();
                urlConnection3.setRequestMethod("POST");
                //urlConnection3.setRequestProperty("Cookie", );
                String cook = SaveSharedPreferences.getCookie(getApplicationContext());

                Log.e("Myapp", "--------------------------- Connected once: ");
                String whatam = SaveSharedPreferences.getUserName(getApplicationContext());
                whatam = "sessionid=" + whatam;
                Log.e("MyApp", "------------------------------- Cookie is   " + whatam);
                urlConnection3.setRequestProperty("Cookie", whatam);
                urlConnection3.setDoOutput(true);
                urlConnection3.setDoInput(true);
                urlConnection3.connect();
                Log.e("Myapp", "--------------------------- Connected twice: ");

                try {
                    OutputStreamWriter wrt = new OutputStreamWriter(urlConnection3.getOutputStream());
                    wrt.write(data);
                    wrt.flush();
                    wrt.close();
                } catch (Exception gh) {
                    Log.e("Myapp", "--------------------------- Error in Writing to Server");
                    return null;
                }
                // Get the server response
                String text="";
                try {
                    if (urlConnection3.getResponseCode() == HttpURLConnection.HTTP_OK) {

                        BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection3.getInputStream()));
                        StringBuffer sb = new StringBuffer("");
                        String line = "";
                        // Read Server Response
                        while ((line = in.readLine()) != null) {
                            sb.append(line);
                        }
                        text = sb.toString();
                        try {
                            in.close();
                        } catch (Exception f) {
                            f.printStackTrace();
                        }
                    } else {
                        return null;
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                    return null;
                }
                JSONArray eventsdates = new JSONArray(text);
                feedbackforms.clear();
                Log.e("Myapp", "--------------------------- Auth:  " + eventsdates.toString());
                //              JSONArray arrofdeadlines = jsonObject2.getJSONArray("deadlines");
                // datetoCourse.put()
                JSONObject json;
                JSONObject internal;
                Date daybeingcoloured;
                String str[] = new String[10];
                for(int i=0;i<10;i++)
                    str[i]="";
                Log.e("Myapp", "--------------------------- Entering For Loop");

                for (int h = 0; h < eventsdates.length(); h++) {
                    json = eventsdates.getJSONObject(h);
                    internal = json.getJSONObject("fields");
                    String day = internal.getString("date");
                    String description = internal.getString("description");
                    String time = internal.getString("time");
                    Boolean feedbackornot = internal.getBoolean("isFeedback");
                    Integer pk = json.getInt("pk");
                    Log.e("Myapp", "ABCDF");
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    daybeingcoloured = sdf.parse(day);
                    String course = internal.getString("course");
                    ArrayList<String> formquery;
                    if(feedbackornot){
                        String form = pk.toString() + "Course: " + course + "  Deadline: " + time;
                        if(feedbackforms.containsKey(daybeingcoloured)){
                            formquery = feedbackforms.get(daybeingcoloured);
                        }
                        else formquery = new ArrayList<>();
                        formquery.add(form);
                        feedbackforms.put(daybeingcoloured, formquery);
                    }
                    else continue;
                    tries=20;
                    //x.add(course);
                    //datetoCourse.remove(daybeingcoloured);
                    //datetoCourse.put(daybeingcoloured, x);
                    //Parse String to Date
                }
                //String mycookie = urlConnection3.getHeaderField("Set-Cookie");
                //SaveSharedPreferences.setCookie(getApplicationContext(), mycookie);
                //                /Log.e("Myapp", "--------------------------- Auth:  " + arrofdeadlines.toString());
                //String newcookie = jsonObject2.getString("Set-Cookie");
                urlConnection3.disconnect();
            } catch (Exception e) {
                // TODO Auto-generated catch block

                e.printStackTrace();
                Log.e("Myapp", "--------------------------- Could not Connect");
            }

        }
               /*       runOnUiThread(new Runnable() {
                public void run(){
                    Toast toast = Toast.makeText(SignIn.this, "Check Network Connection..!!", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.TOP,10,25);
                    toast.show();
                }
            });*/
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        Log.e("Myapp", "--------------------------- Could not Connect");

        for (Date key : feedbackforms.keySet()) {

            ArrayList<String> val = feedbackforms.get(key);

            for(int i=0;i<val.size();i++){
                String X = val.get(i);
                Log.e("STRiNG X    " , X);
            }
            // ...
        }



        //expandableListAdapter=new ExpandableListAdapter(getApplicationContext(), sending);
        //expandableListView.setAdapter(expandableListAdapter);
    }
}
}