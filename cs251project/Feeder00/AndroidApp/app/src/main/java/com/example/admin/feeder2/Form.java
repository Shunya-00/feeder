package com.example.admin.feeder2;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;

public class Form extends AppCompatActivity{

    Button submit;
    LinearLayout formfilling;
    RadioGroup radio1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form);
        radio1 = (RadioGroup) findViewById(R.id.a1);

        submit = (Button) findViewById(R.id.submit);
        //formfilling = (ScrollView) findViewById(R.id.formfilling);
        formfilling.setVisibility(View.GONE);
        radio1 = (RadioGroup) findViewById(R.id.a1);
        int ans = radio1.indexOfChild(findViewById(radio1.getCheckedRadioButtonId()));
        Log.e("Radio Button Working", Integer.toString(ans));


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // int numofquestions 1;
                //for(int i=0 ; i< numofquestions; i++ )
                int radioButtonID = radio1.getCheckedRadioButtonId();
                RadioButton radioButton = (RadioButton) radio1.findViewById(radioButtonID);
                String selectedtext = (String) radioButton.getText();
                Log.e("Radio Button Working", selectedtext);
                //}

                formfilling.setVisibility(View.GONE);
            }
        });

    }

}