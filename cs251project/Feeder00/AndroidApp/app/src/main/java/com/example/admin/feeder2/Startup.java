package com.example.admin.feeder2;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

public class Startup extends AppCompatActivity {
    static public final String MainURL = "http://10.0.2.2:8000/";
    HttpURLConnection urlConnection2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_startup);

        //SaveSharedPreferences.setUserName(getApplicationContext(), "str");
        if (SaveSharedPreferences.getUserName(Startup.this).length() == 0) {
            Log.e("MyApp", "-------------------------- User Had Logged out Last time");
            Intent intent = new Intent(Startup.this, SignIn.class);
            startActivity(intent);
            finish();
            return;
        }
        else{
            Intent gotocalendar = new Intent(Startup.this,Login_Page.class);
            startActivity(gotocalendar);
            finish();
        }
        GetText2 g2 = new GetText2();
        g2.execute();
    }    
        
        public class GetText2 extends AsyncTask {
            // Get user defined values

            @Override
            protected Object doInBackground(Object[] params) {

                 Log.e("Myapp", "--------------------------- Password or Username not filled");
                
                // Create data variable for sent values to server
                    String data = "heyy";
                    try {
                        JSONObject jsonObject2 = new JSONObject();
                        //jsonObject2.put("Set-Cookie", SaveSharedPreferences.getCookie(getApplicationContext()));
                        jsonObject2.put("username", "bharatk");
                        jsonObject2.put("password", "123456789@");
                        data = jsonObject2.toString();
                        Log.e("Myapp", "--------------------------- No error in json: " + data);
                    } catch (Exception ex) {
                        Log.e("Myapp", "---------------------------Error in Creating Json");
                        return null;
                    }
                    try {
            Log.e("Myapp", "--------------------------- Connecting: ");
            //URL url = new URL("http://10.0.2.2:8000/webApp/api/");
            URL url = new URL(MainURL+"webApp/api/signin/");
            urlConnection2 = (HttpURLConnection) url.openConnection();
            urlConnection2.setRequestMethod("POST");
            //String cook = SaveSharedPreferences.getCookie(getApplicationContext());
                        // Log.e("Myapp", "--------------------------- Connected once: ");
            urlConnection2.setRequestProperty("Cookie", SaveSharedPreferences.getUserName(getApplicationContext()));
            urlConnection2.setDoOutput(true);
            urlConnection2.setDoInput(true);
            urlConnection2.connect();
            Log.e("Myapp", "--------------------------- Connected twice: ");

            try {
                OutputStreamWriter wrt = new OutputStreamWriter(urlConnection2.getOutputStream());
                wrt.write(data);
                wrt.flush();
                wrt.close();
            } catch (Exception gh) {
                Log.e("Myapp", "--------------------------- Error in Writing to Server");
                return null;
            }
            // Get the server response
            String text = "abc";
            try {
                if (urlConnection2.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    Log.e("Myapp", "--------------------------- Data is :   " + data);
                    BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection2.getInputStream()));
                    Log.e("Myapp", "--------------------------- Buffered reader is working");
                    StringBuffer sb = new StringBuffer("");
                    Log.e("Myapp", "--------------------------- Buffered reader is working");
                    String line = "";
                    Log.e("Myapp", "--------------------------- Line:  " + line);
                    // Read Server Response
                    while ((line = in.readLine()) != null) {
                        sb.append(line);
                    }
                    text = sb.toString();
                    Log.e("Myapp", "--------------------------- Line:  " + text);
                    try {
                        in.close();
                    } catch (Exception f) {
                        f.printStackTrace();
                        Log.e("Myapp", "--------------------------- Error in Closing Reader");
                    }
                } else {
                    Log.e("Myapp", "--------------------------- Error from Server & response = " + urlConnection2.getResponseCode());
                    return null;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                Log.e("Myapp", "--------------------------- Done with reading input from Server:   " + text);
                return null;
            }
            JSONObject jsonObject2 = new JSONObject(text);
            String authentification = jsonObject2.getString("status");
            Log.e("Myapp", "--------------------------- Auth:  " + authentification);
                        String sessionid = jsonObject2.getString("sessionid");

                        //String newcookie = jsonObject2.getString("Set-Cookie");
            if (authentification.equals("yes")) {
                Log.e("Myapp", "--------------------------- Authentification Successful");
                SaveSharedPreferences.setUserName(getApplicationContext(), sessionid);
                Intent myIntent = new Intent(Startup.this, Login_Page.class);
                startActivity(myIntent);
                finish();
            }
            else{
                SaveSharedPreferences.clearUserName(getApplicationContext());
                urlConnection2.disconnect();
                Intent gotosignin = new Intent(Startup.this, SignIn.class);
                startActivity(gotosignin);
                finish();
                return null;
            }
             urlConnection2.disconnect();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            Log.e("Myapp", "--------------------------- Could not Connect");
                        /*       runOnUiThread(new Runnable() {
                public void run(){
                    Toast toast = Toast.makeText(SignIn.this, "Check Network Connection..!!", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.TOP,10,25);
                    toast.show();
                }
            });*/
                return null;
            }
            return null;
        }
    }
}

