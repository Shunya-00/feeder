package com.example.admin.androidapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class LoggedIn extends AppCompatActivity{

    Button timetable;
    Button calendarvi;
    Button logout;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logged_in);
        logout = (Button) findViewById(R.id.button6);
        calendarvi = (Button) findViewById(R.id.button1);
        timetable = (Button) findViewById(R.id.timetable);
        timetable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent myIntent = new Intent(LoggedIn.this, Timetableact.class);
                startActivity(myIntent);
            }
        });

        calendarvi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent theintent = new Intent(LoggedIn.this, Calender.class);
                startActivity(theintent);
            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SaveSharedPreferences.clearUserName(getApplicationContext());
                Intent theintent = new Intent(LoggedIn.this, MainActivity.class);
                startActivity(theintent);
                finish();
            }
        });

         }

}

