package com.example.admin.androidapp;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import javax.net.ssl.HttpsURLConnection;

import static android.R.attr.password;
import static android.R.string.yes;
import static android.widget.Toast.makeText;
import static com.example.admin.androidapp.R.id.txtPassword;
import static com.example.admin.androidapp.R.id.txtUsername;

public class MainActivity extends AppCompatActivity {

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    //("The App Login Page has started");
    String userna;
    String password;
    EditText txtUsername;
    EditText txtPassword;
    Button Login;
    HttpURLConnection urlConnection = null;
    //ProgressBar spinner;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        Log.e("Myapp", "--------------------------- Page started working");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //spinner = (ProgressBar) findViewById(R.id.progressBar);
        //spinner.setVisibility(View.GONE);
        txtUsername = (EditText) findViewById(R.id.txtUsername);
        txtPassword = (EditText) findViewById(R.id.txtPassword);

        Login = (Button) findViewById(R.id.Login);
        Context c = this;
        txtUsername.requestFocus();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(txtUsername, InputMethodManager.SHOW_IMPLICIT);
        Login.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //spinner.setVisibility(View.VISIBLE);
                Log.e("Myapp", "--------------------------- Login button was clicked");

                userna = txtUsername.getText().toString();
                password = txtPassword.getText().toString();

                try {
                    GetText g = new GetText();
                    g.execute();
                } catch (Exception eee) {
                    Log.e("Myapp", "--------------------------- ");
                }
            }
        });
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    public class GetText extends AsyncTask {
        // Get user defined values

        @Override
        protected Object doInBackground(Object[] params) {

            if (userna.length() == 0) {
                runOnUiThread(new Runnable() {
                    public void run(){
                        Toast toast = Toast.makeText(MainActivity.this, "Please Fill in Username..!!", Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.TOP,10,25);
                        toast.show();
                    }
                });

                Log.e("Myapp", "--------------------------- Password or Username not filled");
                return null;
            } else if (password.length() == 0) {
                runOnUiThread(new Runnable() {
                    public void run(){
                        Toast toast = Toast.makeText(MainActivity.this, "Please Fill in Password..!!", Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.TOP,10,25);
                        toast.show();
                    }
                });
                Log.e("Myapp", "--------------------------- Password or Username not filled");
                return null;
            }
            // Create data variable for sent values to server
            else {
                String data = "heyy";

                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("username", userna);
                    jsonObject.put("password", password);
                    jsonObject.put("csrftoken", SaveSharedPreferences.getUserName(getApplicationContext()));
                    data = jsonObject.toString();
                    Log.e("Myapp", "--------------------------- No error in json: " + data);
                } catch (Exception ex) {
                    Log.e("Myapp", "---------------------------Error in Creating Json");
                    return null;
                }

                // Send data
                // URL url = new URL("http://10.0.1.7:8000/webApp/api/);
                //String ur = url.toString();

                try {
                    Log.e("Myapp", "--------------------------- Connecting: ");
                    URL url = new URL("http://10.0.2.2:8000/webApp/api/");
//                    URL url = new URL("http://192.168.0.113:8000/webApp/api/");
                    urlConnection = (HttpURLConnection) url.openConnection();
                    urlConnection.setRequestMethod("POST");
                    urlConnection.setDoOutput(true);
                    urlConnection.setDoInput(true);
                    urlConnection.connect();
                    Log.e("Myapp", "--------------------------- Connected: ");

                    try {
                        OutputStreamWriter wrt = new OutputStreamWriter(urlConnection.getOutputStream());
                        wrt.write(data);
                        wrt.flush();
                        wrt.close();
                    } catch (Exception gh) {
                        Log.e("Myapp", "--------------------------- Error in Writing to Server");
                        return null;
                    }
                    // Get the server response
                    String text = "abc";
                    try {
                        if (urlConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                            Log.e("Myapp", "--------------------------- Data is :   " + data);
                            BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                            Log.e("Myapp", "--------------------------- Buffered reader is working");
                            StringBuffer sb = new StringBuffer("");
                            Log.e("Myapp", "--------------------------- Buffered reader is working");
                            String line = "";
                            Log.e("Myapp", "--------------------------- Line:  " + line);
                            // Read Server Response
                            while ((line = in.readLine()) != null) {
                                // Append server response in string
                                sb.append(line);
                            }
                            text = sb.toString();
                            Log.e("Myapp", "--------------------------- Line:  " + text);
                            try {
                                in.close();
                            } catch (Exception f) {
                                f.printStackTrace();
                                Log.e("Myapp", "--------------------------- Error in Closing Reader");
                            }
                        } else {
                            Log.e("Myapp", "--------------------------- Error from Server & response = " + urlConnection.getResponseCode());
                            return null;
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        Log.e("Myapp", "--------------------------- Done with reading input from Server:   " + text);
                        return null;
                    }
                    JSONObject jsonObject2 = new JSONObject(text);
                    String authentification = jsonObject2.getString("status");
                    Log.e("Myapp", "--------------------------- Auth:  " + authentification);
                    //String token = jsonObject2.getString("csrf_token");
                    //Log.e("Myapp", "--------------------------- Token:  " + token);

                    if (authentification.equals("yes")) {
                        Log.e("Myapp", "--------------------------- Authentification Successful");
                        SaveSharedPreferences.setUserName(getApplicationContext(), userna);
                        Intent myIntent = new Intent(MainActivity.this, LoggedIn.class);
                        startActivity(myIntent);
                        finish();
                    } else {
                        Log.e("Myapp", "--------------------------- Authentification Unsuccessful");
                        runOnUiThread(new Runnable() {
                            public void run(){
                                Toast toast = Toast.makeText(MainActivity.this, "Invalid Credentials..!!", Toast.LENGTH_LONG);
                                toast.setGravity(Gravity.TOP,10,25);
                                toast.show();
                            }
                        });
                        return null;
                    }

                    //urlConnection.setRequestProperty("Content-Type", "application/json");
                    //urlConnection.setRequestProperty("Accept", "application/json");

                    urlConnection.disconnect();

                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    Log.e("Myapp", "--------------------------- Could not Connect");
                    return null;
                }
                return null;
            }
        }
    }
}
