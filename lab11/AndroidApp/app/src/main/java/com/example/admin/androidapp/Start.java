package com.example.admin.androidapp;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;

public class Start extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        //SaveSharedPreferences.setUserName(getApplicationContext(),"a");
        if (SaveSharedPreferences.getUserName(Start.this).length() == 0) {
            Log.e("MyApp", "-------------------------- User Had Logged out Last time");
            Intent intent = new Intent(Start.this, MainActivity.class);
            startActivity(intent);
            finish();
            return;
        } else {
            Intent intent = new Intent(Start.this, LoggedIn.class);
            startActivity(intent);
            finish();
            return;
        }
    }
}
