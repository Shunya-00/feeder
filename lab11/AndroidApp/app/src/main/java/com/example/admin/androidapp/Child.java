package com.example.admin.androidapp;

/**
 * Created by ADMIN on 11/1/2016.
 */

public class Child{
    public String childId;
    public String childName;
    public Child(String chiledId, String childName) {
        super();
        this.childId = chiledId;
        this.childName = childName;
    }
    public String getChiledId() {
        return childId;
    }
    public void setChiledId(String chiledId) {
        this.childId = chiledId;
    }
    public String getChildName() {
        return childName;
    }
    public void setChildName(String childName) {
        this.childName = childName;
    }
}
