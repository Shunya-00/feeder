package com.example.admin.androidapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.icu.util.Calendar;
import android.net.Uri;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.Toast;
import android.widget.ExpandableListView;

import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;
import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CaldroidListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Queue;

import static android.R.attr.button;
import static com.example.admin.androidapp.R.color.blue;
import static com.example.admin.androidapp.R.color.caldroid_black;
import static com.example.admin.androidapp.R.color.caldroid_sky_blue;
import static com.example.admin.androidapp.R.color.orange;
import static com.example.admin.androidapp.R.color.white;
import static com.example.admin.androidapp.R.drawable.button_ori;
import static com.example.admin.androidapp.R.drawable.button_unori;

public class Calender extends AppCompatActivity {
    ExpandibleListAdapter expandableListAdapter;
    Button Logout;
    Button Home;
    /*expandable list*/
    ExpandableListView abc;
    /*list items*/
    ArrayList<Group> groups = new ArrayList<Group>();
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e("AppLaunches", "Nice");

        Log.e("AppLaunches", "SuperNice");

        setContentView(R.layout.activity_calender);

        Logout = (Button) findViewById(R.id.logoutbttn);
        Logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                SaveSharedPreferences.clearUserName(getApplicationContext());
                Intent myIntent = new Intent(Calender.this, MainActivity.class);
                startActivity(myIntent);
                finish();
            }
        });

        Home = (Button) findViewById(R.id.homebutton);
        Home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                Intent myIntent = new Intent(Calender.this, LoggedIn.class);
                startActivity(myIntent);
            }
        });

        final CaldroidFragment caldroidFragment = new CaldroidFragment();
        //Bundle args = new Bundle();
        //args.putInt(CaldroidFragment.THEME_RESOURCE, com.caldroid.R.style.CaldroidDefault);
         //caldroidFragment.setArguments(args);
        FragmentTransaction t = getSupportFragmentManager().beginTransaction();
        t.replace(R.id.calenderview, caldroidFragment);
        t.commit();
        //caldroidFragment.saveStatesToKey(savedInstanceState, "original");
        Date Lastdate = new Date(System.currentTimeMillis());
        caldroidFragment.setTextColorForDate(orange, Lastdate);

        final CaldroidListener listener = new CaldroidListener(){
            Date lastdate = new Date(System.currentTimeMillis());
            @Override
            public final void onSelectDate(Date date, View view){

                caldroidFragment.setTextColorForDate(caldroid_black, lastdate);
                caldroidFragment.setTextColorForDate(orange, date);
                lastdate = date;
                caldroidFragment.refreshView();
            }
        };
        caldroidFragment.setCaldroidListener(listener);
        genarateData();

     /*instantiate adapter with our item list*/
        expandableListAdapter = new ExpandibleListAdapter(this, groups);

     /*we get list view*/
        abc = (ExpandableListView) findViewById(R.id.accord);

        /*set adapter to list view*/
        abc.setAdapter(expandableListAdapter);
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
    }

    public void genarateData() {
        Group group;
        for (int i = 0; i < 4; i++) {

            ArrayList<Child> childrens = new ArrayList<Child>();
            childrens.clear();
            Child child;
            for (int j = 0; j < 2; j++) {
                child = new Child("" + j, "I am Child " + j);
                childrens.add(child);
            }
            group = new Group("" + i, "I am Group " + i, childrens);
            groups.add(group);
        }
    }
}
